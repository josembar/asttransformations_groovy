package ast
import ast.Customer

Date rightNow = new Date()

Customer Customer1 = new Customer(first: 'José',last: 'Barrantes',age: 25,since: rightNow,favItems: ['beer','sex'])
Customer Customer2 = new Customer('José','Barrantes',25,rightNow,['beer','sex'])

assert Customer1 == Customer2

//Customer1.first = 'Manu'
